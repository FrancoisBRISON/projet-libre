package com.example.projetlibre;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import mdt.Head;
import mdt.Rule;
import mdt.Tape;

public class HeadUTest
{
    @Test
    public void executeStep()
    {
        Tape tape = new Tape();

        tape.addSymbol('a');
        tape.addSymbol('b');
        tape.addSymbol('c');

        tape.setCurrentCellIndex(1);

        Rule rule = new Rule(1, 'b', 2, '*', 1);

        Head head = new Head();
        head.executeRule(tape, rule);

        assertEquals('*', tape.getSymbolAt(1));
        assertEquals(2, tape.getCurrentCellIndex());
    }
}
