package mdt;

import java.util.ArrayList;

public class Tape
{
    // --- --- --- --- --- ATTRIBUTES --- --- --- --- --- //

    private ArrayList<Character> symbolsList;

    private int currentCellIndex;

    // --- --- --- --- --- CONSTRUCTORS --- --- --- --- --- //

    public Tape() { this(new ArrayList<Character>()); }

    public Tape(ArrayList<Character> _symbolsList) { setSymbolsList(_symbolsList); }

    // --- --- --- --- --- GETTERS --- --- --- --- --- //

    public ArrayList<Character> getSymbolsList() { return symbolsList; }

    public char getCurrentSymbol() { return symbolsList.get(getCurrentCellIndex()); }

    public int getCurrentCellIndex() { return currentCellIndex; }

    public char getSymbolAt(int index)
    {
        if(isIndexInBounds(index) == false) return '\0';
        return this.symbolsList.get(index);
    }

    // --- --- --- --- --- SETTERS --- --- --- --- --- //

    public void setSymbolsList(ArrayList<Character> _symbolsList)
    {
        this.symbolsList = _symbolsList;
        setCurrentCellIndex(getCurrentCellIndex());
    }

    public void setCurrentCellIndex(int currentCellIndex)
    {
        this.currentCellIndex = currentCellIndex;
        if(this.currentCellIndex < 0) this.currentCellIndex = 0;
        if(this.currentCellIndex > this.getSymbolsList().size() - 1) this.currentCellIndex = this.getSymbolsList().size() - 1;
    }

    public void addSymbol(Character c) { this.symbolsList.add(c); }

    public void removeSymbol(Character c) { this.symbolsList.remove(c); }

    public boolean removeSymbolAt(int index)
    {
        if(isIndexInBounds(index) == false) return false;
        this.symbolsList.remove(index);
        return true;
    }

    public void setCurrentSymbol(char symbol) { this.symbolsList.set(currentCellIndex, symbol); }

    // --- --- --- --- --- METHODS --- --- --- --- --- //

    public boolean isIndexInBounds(int index)
    {
        if(index < 0 || index > this.getSymbolsList().size() - 1) return false;
        return true;
    }
}
