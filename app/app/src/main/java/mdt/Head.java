package mdt;

import java.util.ArrayList;

public class Head
{
    // --- --- --- --- --- ATTRIBUTES  --- --- --- --- --- //

    private int state;

    // --- --- --- --- --- CONSTRUCTORS  --- --- --- --- --- //

    public Head()
    {
        this(0);
    }

    public Head(int _state)
    {
        setState(_state);
    }

    // --- --- --- --- --- GETTERS  --- --- --- --- --- //

    /**
     * @return the state
     */
    public int getState() {
        return state;
    }

    // --- --- --- --- --- SETTERS  --- --- --- --- --- //

    /**
     * @param state the state to set
     */
    public void setState(int state) {
        this.state = state;
    }

    // --- --- --- --- --- METHODS  --- --- --- --- --- //

    /**
     * Apply a Rule to a Tape. This will change the current symbol to the action attribute of the Rule, change the current state to the final state attribute of the Rule,
     * and move the current cell index of the tape.
     * @param tape Tape to edit.
     * @param rule Rule to apply.
     */
    public void executeRule(Tape tape, Rule rule)
    {
        setState(rule.getFinalState());
        tape.setCurrentSymbol(rule.getAction());
        tape.setCurrentCellIndex(tape.getCurrentCellIndex() + rule.getMovement());
    }

}
