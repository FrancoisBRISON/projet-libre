package mdt;

import java.util.ArrayList;

public class Machine
{
    // --- --- --- --- --- ATTRIBUTES --- --- --- --- --- //

    private ArrayList<Rule> rules;

    private Head head;

    private Tape tape;

    // --- --- --- --- --- CONSTRUCTORS --- --- --- --- --- //

    public Machine()
    {
        this(new ArrayList<Rule>(), new Head(), new Tape());
    }

    public Machine(ArrayList<Rule>_rules, Head _head, Tape _tape)
    {
        setRules(_rules);
        setHead(_head);
        setTape(_tape);
    }

    // --- --- --- --- --- GETTERS --- --- --- --- --- //

    public ArrayList<Rule> getRules() { return rules; }

    public Head getHead() { return head; }

    public Tape getTape() { return tape; }

    public Rule getRuleAt(int index)
    {
        if(isIndexInBounds(index) == false) return null;
        return rules.get(index);
    }

    // --- --- --- --- --- SETTERS --- --- --- --- --- //

    public void setRules(ArrayList<Rule> rules) { this.rules = rules; }

    public void setHead(Head head) { this.head = head; }

    public void setTape(Tape tape) { this.tape = tape; }

    public boolean setRuleAt(Rule rule, int index)
    {
        if(isIndexInBounds(index) == false) return false;
        rules.set(index, rule);
        return true;
    }

    public void addRule(Rule rule) { rules.add(rule); }

    public boolean removeRuleAt(int index)
    {
        if(isIndexInBounds(index) == false) return false;
        rules.remove(index);
        return true;
    }

    // --- --- --- --- --- METHODS --- --- --- --- --- //

    public boolean isIndexInBounds(int index)
    {
        if(index < 0 || index > rules.size() - 1) return false;
        return true;
    }

    /**
     * Method returning the Rule from the list of Rule objects "rules" matching the current tape symbol "symbol" and the current head state "state".
     * @param symbol inputSymbol of the Rule to return.
     * @param state initialState of the Rule to return.
     * @return A Rule object whose inputSymbol attribute is symbol, and whose initialState is state from the rules list; null if such a Rule is not found.
     */
    public Rule findRule(char symbol, int state)
    {
        for(Rule rule : this.rules)
        {
            if(rule.getInputSymbol() == symbol && rule.getInitialState() == state) return rule;
        }
        return null;
    }

    /**
     * Application of the findRule method to this Machine's currentSymbol of the tape and to the state of the head.
     * @return A Rule object whose inputSymbol attribute is the current tape symbol, and whose initialState is the current head state from the rules list;
     * null if such a Rule is not found.
     */
    public Rule findCurrentRule()
    {
        return findRule(this.tape.getCurrentSymbol(), this.head.getState());
    }

    /**
     * Method executing one step of the Turing Machine (apply one Rule to the current state, then stop).
     * @return The applied Rule (null if not rule applied).
     */
    public Rule executeOneStep()
    {
        // find the rule to apply
        Rule ruleToApply = findCurrentRule();

        // if there is no matching rule ?
        if(ruleToApply == null)
        {

        }
        else // if there is a rule to apply, apply it on the tape
        {
            this.head.executeRule(this.tape, ruleToApply);
        }
        return ruleToApply;
    }

    /**
     * Method executing Rules until none can be found.
     */
    public void executeAllSteps()
    {
        Rule r = findCurrentRule();
        while(r != null)
        {
            this.head.executeRule(this.tape, r);
            r = findCurrentRule();
        }
    }
}
