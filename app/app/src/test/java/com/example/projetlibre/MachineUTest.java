package com.example.projetlibre;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import mdt.Head;
import mdt.Machine;
import mdt.Rule;
import mdt.Tape;

public class MachineUTest
{
    @Test
    public void findRule()
    {
        Machine m = new Machine();

        Rule r1 = new Rule(0, 'a', 0, 'b', 0);
        Rule r2 = new Rule(0, 'b', 0, 'b', 0);
        Rule r3 = new Rule(1, 'c', 0, 'b', 0);

        m.addRule(r1);
        m.addRule(r2);
        m.addRule(r3);

        m.setHead(new Head(0));

        Tape tape = new Tape();

        tape.addSymbol('a');
        tape.addSymbol('b');
        tape.addSymbol('c');
        tape.setCurrentCellIndex(1);

        m.setTape(tape);

        assertEquals(r2, m.findCurrentRule());

        m.getHead().setState(1);
        m.getTape().setCurrentCellIndex(2);

        assertEquals(r3, m.findCurrentRule());

        m.getTape().setCurrentCellIndex(1);

        assertNull(m.findCurrentRule());
    }

    @Test
    public void executeOnStep()
    {
        Machine m = new Machine();

        Rule r1 = new Rule(0, 'a', 0, 'b', 0);
        Rule r2 = new Rule(0, 'b', 1, '*', 1);
        Rule r3 = new Rule(1, 'c', 4, '+', -2);

        m.addRule(r1);
        m.addRule(r2);
        m.addRule(r3);

        m.setHead(new Head(0));

        Tape tape = new Tape();

        tape.addSymbol('a');
        tape.addSymbol('b');
        tape.addSymbol('c');
        tape.setCurrentCellIndex(1);

        m.setTape(tape);

        m.executeOneStep();
        assertEquals('*', m.getTape().getSymbolAt(1));
        assertEquals(2, m.getTape().getCurrentCellIndex());
        assertEquals(1, m.getHead().getState());


        m.executeOneStep();
        assertEquals('+', m.getTape().getSymbolAt(2));
        assertEquals(0, m.getTape().getCurrentCellIndex());
        assertEquals(4, m.getHead().getState());

        assertNull(m.executeOneStep());
    }

    @Test
    public void executeAllSteps()
    {
        Machine m = new Machine();

        Rule r1 = new Rule(0, 'a', 0, 'b', 0);
        Rule r2 = new Rule(0, 'b', 1, '*', 1);
        Rule r3 = new Rule(1, 'c', 4, '+', -2);

        m.addRule(r1);
        m.addRule(r2);
        m.addRule(r3);

        m.setHead(new Head(0));

        Tape tape = new Tape();

        tape.addSymbol('a');
        tape.addSymbol('b');
        tape.addSymbol('c');
        tape.setCurrentCellIndex(1);

        m.setTape(tape);

        m.executeAllSteps();

        assertEquals('a', m.getTape().getSymbolAt(0));
        assertEquals('*', m.getTape().getSymbolAt(1));
        assertEquals('+', m.getTape().getSymbolAt(2));

        assertEquals(4, m.getHead().getState());
        assertEquals(0, m.getTape().getCurrentCellIndex());
    }
}
