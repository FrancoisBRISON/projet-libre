package com.example.projetlibre;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import mdt.Tape;

public class TapeUTest
{
    @Test
    void buildTape()
    {
        Tape tape = new Tape();

        tape.addSymbol('a');
        tape.addSymbol('b');
        tape.addSymbol('c');

        assertEquals('a', tape.getSymbolsList().get(0));
        assertEquals('b', tape.getSymbolsList().get(1));
        assertEquals('c', tape.getSymbolsList().get(2));
    }

    @Test
    void removeSymbolAt()
    {
        Tape tape = new Tape();

        tape.addSymbol('a');
        tape.addSymbol('b');
        tape.addSymbol('c');

        tape.removeSymbolAt(2);

        assertEquals(2, tape.getSymbolsList().size());

        assertEquals('a', tape.getSymbolsList().get(0));
        assertEquals('b', tape.getSymbolsList().get(1));

        tape.removeSymbolAt(-1);
        tape.removeSymbolAt(2);

        assertEquals('a', tape.getSymbolsList().get(0));
        assertEquals('b', tape.getSymbolsList().get(1));
    }

    @Test
    void setCurrentSymbol()
    {
        Tape tape = new Tape();

        tape.addSymbol('a');
        tape.addSymbol('b');
        tape.addSymbol('c');

        tape.setCurrentCellIndex(1);
        tape.setCurrentSymbol('*');

        assertEquals(1, tape.getCurrentCellIndex());

        assertEquals('a', tape.getSymbolsList().get(0));
        assertEquals('*', tape.getSymbolsList().get(1));
        assertEquals('c', tape.getSymbolsList().get(2));

        assertEquals('*', tape.getCurrentSymbol());
    }

    @Test
    void setCurrentCellIndex()
    {
        Tape tape = new Tape();

        tape.addSymbol('a');
        tape.addSymbol('b');
        tape.addSymbol('c');

        tape.setCurrentCellIndex(-10);
        assertEquals(0, tape.getCurrentCellIndex());

        tape.setCurrentCellIndex(10);
        assertEquals(2, tape.getCurrentCellIndex());

        tape.setCurrentCellIndex(3);
        assertEquals(2, tape.getCurrentCellIndex());

        tape.setCurrentCellIndex(0);
        assertEquals(0, tape.getCurrentCellIndex());
    }
}
