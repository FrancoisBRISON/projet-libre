package mdt;

public class Rule
{

    // --- --- --- --- --- ATTRIBUTES --- --- --- --- --- //

    // INPUT
    private int initialState;
    private Character inputSymbol;

    // OUTPUT
    private int finalState;
    private Character action;
    private int movement;

    public Rule()
    {
        this(-1, '\0', -1, '\0', 0);
    }

    public Rule(int _initialState, char _inputSymbol, int _finalState, char _action, int _movement)
    {
        setInitialState(_initialState);
        setInputSymbol(_inputSymbol);
        setFinalState(_finalState);
        setAction(_action);
        setMovement(_movement);
    }


    // --- --- --- --- --- GETTERS --- --- --- --- --- //
    /**
     * @return the initialState
     */
    public int getInitialState() {
        return initialState;
    }
    /**
     * @return the inputSymbol
     */
    public Character getInputSymbol() {
        return inputSymbol;
    }
    /**
     * @return the finalState
     */
    public int getFinalState() {
        return finalState;
    }
    /**
     * @return the action
     */
    public Character getAction() {
        return action;
    }
    /**
     * @return the movement
     */
    public int getMovement() {
        return movement;
    }

    // --- --- --- --- --- SETTERS --- --- --- --- --- //

    /**
     * @param initialState the initialState to set
     */
    public void setInitialState(int initialState) {
        this.initialState = initialState;
    }
    /**
     * @param inputSymbol the inputSymbol to set
     */
    public void setInputSymbol(Character inputSymbol) {
        this.inputSymbol = inputSymbol;
    }
    /**
     * @param finalState the finalState to set
     */
    public void setFinalState(int finalState) {
        this.finalState = finalState;
    }
    /**
     * @param action the action to set
     */
    public void setAction(Character action) {
        this.action = action;
    }
    /**
     * @param movement the movement to set
     */
    public void setMovement(int movement) {
        this.movement = movement;
    }

    // --- --- --- --- --- METHODS --- --- --- --- --- //

    public void printRule()
    {
        System.out.println("If\t" + "state is : " + Integer.toString(initialState) + "\t" + "symbol is : " + inputSymbol);
        System.out.println(">Then\t" + "state is : " + Integer.toString(finalState) + "\t" + "symbol is : " + action + "\t" + "movement is : " + Integer.toString(movement));
    }
}
